import { ImageAction } from '../helpers/create-reducer';
import { AppState } from './types';
declare const Reducer: (state: AppState, action: ImageAction) => AppState;
export default Reducer;
