import type { OpenDialogSyncOptions, SaveDialogOptions } from 'electron';

import {
	clearModifiedImagesAction,
	setColorAtPixelAction,
	setModificationInProgressAction,
	setModifiedImagesAction,
	setOriginalImageAction,
} from '../store/actions';
import store from '../store/store';
import {
	ElectronCallFunction,
	ElectronCallFunctionRequest,
	FrontendImageInfo,
	RendererCallFunction,
	RendererCallFunctionRequest,
} from '../../../shared/types';
import toastService from './toast-service';
import { AlertType } from '../components/Alert/Alert';
import { CustomError } from '../../../shared/error';
import { Dimensions } from '../../../shared/types';
import type { ImageModificationInfo } from '../../../shared/modifications/types';

// Loaded in the preload.index.ts file
const ipcRenderer = (window as Window)?.electron?.ipcRenderer;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const callableFunctions: Record<RendererCallFunction, (...props: any) => void> = {
	setOriginalImage: (base64: string, dimensions: Dimensions) => {
		store.dispatch(
			setOriginalImageAction({
				base64,
				dimensions,
			})
		);
		store.dispatch(clearModifiedImagesAction());
	},
	setModifiedImages: (imageInfos: FrontendImageInfo[]) => {
		store.dispatch(setModifiedImagesAction(imageInfos));
	},
	clearModifiedImages: () => {
		store.dispatch(clearModifiedImagesAction());
	},
	setModificationInProgress: (inProgress: boolean) => {
		store.dispatch(setModificationInProgressAction(inProgress));
	},
	showNotification: (message: string, status: AlertType, innerException: CustomError) => {
		(toastService[status] || toastService['info'])(message, { innerException });
	},
	setColorAtPixel: (color: string) => {
		store.dispatch(setColorAtPixelAction(color));
	},
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
ipcRenderer?.on('mainprocess-response', (event: any, arg: RendererCallFunctionRequest) => {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	const func: ((...args: any[]) => void) | undefined = (callableFunctions as any)[arg.func];
	if (func) {
		try {
			func(...arg.funcArgs);
		} catch (err) {
			console.error(
				'Failed to execute renderer function that was requested from the main thread',
				{ event, arg, func }
			);
		}
	} else {
		console.error("Received func request on mainprocess-response, but function doesn't exist", {
			event,
			arg,
		});
	}
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
async function callMainFunction(name: ElectronCallFunction, args: any[]): Promise<any> {
	return await ipcRenderer?.invoke('request-mainprocess-action', {
		func: name,
		funcArgs: args,
	} as ElectronCallFunctionRequest);
}

export function openImage(options: OpenDialogSyncOptions = {}): Promise<void> {
	return callMainFunction('openImage', [options]);
}

export function saveImage(options: SaveDialogOptions = {}): Promise<void> {
	return callMainFunction('saveImage', [options]);
}

export function applyModifications(modifications: ImageModificationInfo[]) {
	return callMainFunction('applyModifications', [modifications]);
}

export function setLanguage(language: string): Promise<void> {
	return callMainFunction('setLanguage', [language]);
}

export function getColorAtPixel(x: number, y: number): Promise<string> {
	return callMainFunction('getColorAtPixel', [x, y]);
}
