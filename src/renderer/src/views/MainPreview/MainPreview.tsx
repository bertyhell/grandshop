import { useTranslation } from 'react-i18next';
import React, {
	createRef,
	FunctionComponent,
	MutableRefObject,
	RefObject,
	useEffect,
	useRef,
} from 'react';
import 'viewerjs/dist/viewer.css';
import Viewer from 'viewerjs';

import './MainPreview.scss';
import {
	selectModificationInProgress,
	selectModifiedImage,
	selectOriginalImage,
} from '../../store/selectors';
import { connect } from 'react-redux';
import { AppState } from '../../store/types';
import Spinner from '../../components/Spinner/Spinner';
import { getImageSource } from '../../helpers/base64';
import { FrontendImageInfo } from '../../../../shared/types';

interface MainPreviewProps {
	originalImage: FrontendImageInfo | null;
	modifiedImage: FrontendImageInfo | null | undefined;
	modificationInProgress: boolean;
}

const MainPreview: FunctionComponent<MainPreviewProps> = ({
	originalImage,
	modifiedImage,
	modificationInProgress,
}) => {
	const [t] = useTranslation();

	const el: RefObject<HTMLImageElement> = createRef<HTMLImageElement>();
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	const viewer: MutableRefObject<any | null> = useRef<any | null>(null);

	const updateViewer = () => {
		if (viewer.current) {
			viewer.current.update();
		}
	};

	useEffect(() => {
		if (!el.current || viewer.current) {
			return;
		}
		const DEFAULT_BUTTON = {
			show: true,
			size: 'large' as const,
		};
		viewer.current = new Viewer(el.current, {
			inline: true,
			backdrop: false,
			button: false,
			navbar: false,
			title: false,
			toolbar: {
				zoomIn: DEFAULT_BUTTON,
				zoomOut: DEFAULT_BUTTON,
				oneToOne: 0,
				reset: DEFAULT_BUTTON,
				prev: 0,
				play: 0,
				next: 0,
				rotateLeft: DEFAULT_BUTTON,
				rotateRight: DEFAULT_BUTTON,
				flipHorizontal: DEFAULT_BUTTON,
				flipVertical: DEFAULT_BUTTON,
			},
			transition: false,
			fullscreen: false,
			movable: false,
			rotatable: true,
			scalable: true,
			zIndexInline: 1,
		});

		// Listen for preview div size changes, so we can update the viewer
		new ResizeObserver(updateViewer).observe(el.current);
	}, [el]);

	useEffect(() => {
		updateViewer();
	}, [modifiedImage]);

	return (
		<div className="m-main-preview">
			{(modifiedImage || originalImage) && (
				<img
					className="o-image-viewer"
					ref={el}
					src={getImageSource(modifiedImage?.base64 || originalImage?.base64)}
					alt="Een voorbeeld van de aangepaste afbeelding"
				/>
			)}
			{modificationInProgress && (
				<div className="o-spinner-wrapper">
					<Spinner />
					{t('views/main-preview/main-preview___image-is-being-updated')}
				</div>
			)}
		</div>
	);
};

const mapStateToProps = (state: { app: AppState }) => ({
	originalImage: selectOriginalImage(state),
	modifiedImage: selectModifiedImage(state),
	modificationInProgress: selectModificationInProgress(state),
});

export default connect(mapStateToProps)(MainPreview);
