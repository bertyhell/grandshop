/**
 This script runs over all the code and looks for either:
 <Trans>Request</Trans>
 or
 t('scripts/extract-and-replace-translations___formatted-key')

 and replaces them with:
 <Trans i18nKey="testdir1/testdir2/testfile1______request">Request</Trans>
 or
 t('scripts/extract-and-replace-translations___formatted-key')


 and it also outputs a json file with the translatable strings:
 {
  "testdir1/testdir2/testfile1___request": "Request"
 }

 Every time the `npm run extract-translations` command is run, it will extract new translations that it finds
 (without i18nKey or not containing "___")
 and add them to the json file without overwriting the existing strings.
 */

import * as fs from 'fs';
import * as glob from 'glob-promise';
import * as _ from 'lodash';
import * as path from 'path';
import * as localTranslations from '../shared/translations/en.json';

type keyMap = { [key: string]: string };

const oldTranslations: keyMap = localTranslations;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
function sortObject(object: any): any {
	const objectKeys = Object.keys(object);
	return objectKeys.sort().reduce((total, key) => {
		if (objectKeys.indexOf(key) !== -1) {
			total[key] = object[key];
		}
		return total;
	}, {});
}

function getFormattedKey(filePath: string, key: string) {
	const fileKey = filePath
		.replace(/[\\/]+/g, '/')
		.split('.')[0]
		.split(/[\\/]/g)
		.map((part) => _.kebabCase(part))
		.join('/')
		.toLowerCase()
		.replace(/(^\/+|\/+$)/g, '')
		.trim();
	const formattedKey = _.kebabCase(key);
	return `${fileKey}___${formattedKey}`;
}

function getFormattedTranslation(translation: string) {
	return translation.trim().replace(/\t\t(\t)+/g, ' ');
}

async function getFilesByGlob(globPattern: string): Promise<string[]> {
	const options = {
		ignore: ['**/*.d.ts', 'scripts/**/*.*'],
		cwd: path.join(__dirname, '../'),
	};
	return await glob(globPattern, options);
}

function extractTranslationsFromCodeFiles(codeFiles: string[]) {
	const newTranslations: keyMap = {};
	// Find and extract translations, replace strings with translation keys
	codeFiles.forEach((relativeFilePath: string) => {
		try {
			const absoluteFilePath = path.resolve(__dirname, '../', relativeFilePath);
			let content: string = fs.readFileSync(absoluteFilePath).toString();

			// Replace t() functions ( including i18n.t() )
			content = content.replace(
				// Match char before t function to make sure it isn't part of a bigger function name, eg: sent()
				/([^a-zA-Z])t\(\s*'([\s\S]+?)'([^)]*)\)/g,
				(match: string, prefix: string, translation: string, translationParams: string) => {
					let formattedKey: string | undefined;
					const formattedTranslation: string = getFormattedTranslation(translation);
					if (formattedTranslation.includes('___')) {
						formattedKey = formattedTranslation;
					} else {
						formattedKey = getFormattedKey(relativeFilePath, formattedTranslation);
					}
					if (translationParams.includes('(')) {
						console.warn(
							'WARNING: Translation params should not contain any function calls, ' +
								'since the regex replacement cannot deal with brackets inside the t() function. ' +
								'Store the translation params in a variable before calling the t() function.',
							{
								match,
								prefix,
								translation,
								translationParams,
								absoluteFilePath,
							}
						);
					}
					// If translation contains '___', use original translation, otherwise use translation found by the regexp
					newTranslations[formattedKey] =
						(formattedTranslation.includes('___')
							? (oldTranslations as keyMap)[formattedKey]
							: formattedTranslation) || '';
					return `${prefix}t('${formattedKey}'${translationParams})`;
				}
			);

			fs.writeFileSync(absoluteFilePath, content);
		} catch (err) {
			console.error(`Failed to find translations in file: ${relativeFilePath}`, err);
		}
	});
	return newTranslations;
}

async function getOnlineTranslations() {
	try {
		// Read file from poeditor website under /src/translations/poeditor/project-id/en.json
		const poEditorFiles = await getFilesByGlob('shared/translations/poeditor/*/en.json');
		const poEditorFile: string = poEditorFiles[0];
		if (!poEditorFile) {
			throw new Error(
				'File fetched from poEditor website could not be found: /src/shared/translations/poeditor/*/en.json'
			);
		}
		try {
			const filePath = path.resolve(__dirname, '../src/', poEditorFile);
			return JSON.parse(fs.readFileSync(filePath).toString());
		} catch (err) {
			throw new Error(
				`Failed to parse json file from poeditor: ${JSON.stringify(err, null, 2)}`
			);
		}
	} catch (err) {
		// Still return empty translations file to continue with the rest of the extraction script
		return {};
	}
}

function checkTranslationsForKeysAsValue(translationJson: string) {
	// Identify  if any translations contain "___", then something went wrong with the translations
	const faultyTranslations = [];
	const faultyTranslationRegexp = /"(.*___.*)": ".*___/g;
	let matches: RegExpExecArray | null;
	do {
		matches = faultyTranslationRegexp.exec(translationJson);
		if (matches) {
			faultyTranslations.push(matches[1]);
		}
	} while (matches);

	if (faultyTranslations.length) {
		throw new Error(`Failed to extract translations, the following translations would be overridden by their key:
\t${faultyTranslations.join('\n\t')}`);
	}
}

async function updateTranslations() {
	const onlineTranslations = await getOnlineTranslations();

	// Extract translations from code and replace code by reference to translation key
	const codeFiles = await getFilesByGlob('**/*.@(ts|tsx)');
	const newTranslations: keyMap = extractTranslationsFromCodeFiles(codeFiles);

	// Compare existing translations to the new translations
	const oldTranslationKeys: string[] = _.keys(oldTranslations);
	const newTranslationKeys: string[] = _.keys(newTranslations);
	const addedTranslationKeys: string[] = _.without(newTranslationKeys, ...oldTranslationKeys);
	const removedTranslationKeys: string[] = _.without(oldTranslationKeys, ...newTranslationKeys);
	const existingTranslationKeys: string[] = _.intersection(
		newTranslationKeys,
		oldTranslationKeys
	);

	// Console log translations that were found in the json file but not in the code
	console.warn(
		`The following translation keys were removed:
\t${removedTranslationKeys.join('\n\t')}`
	);

	// Combine the translations in the json with the freshly extracted translations from the code
	const combinedTranslations: keyMap = {};
	existingTranslationKeys.forEach((key: string) => {
		combinedTranslations[key] = onlineTranslations[key] || oldTranslations[key];
	});
	addedTranslationKeys.forEach((key: string) => {
		combinedTranslations[key] = onlineTranslations[key] || newTranslations[key];
	});

	const nlJsonContent = JSON.stringify(sortObject(combinedTranslations), null, 2);
	checkTranslationsForKeysAsValue(nlJsonContent); // Throws error if any key is found as a value

	fs.writeFileSync(
		`${__dirname.replace(/\\/g, '/')}/../shared/translations/en.json`,
		nlJsonContent
	);

	const totalTranslations = existingTranslationKeys.length + addedTranslationKeys.length;
	console.info(
		`Wrote ${totalTranslations} src/shared/translations/en.json file
\t${addedTranslationKeys.length} translations added
\t${removedTranslationKeys.length} translations deleted`
	);
}

updateTranslations().catch((err) => console.error('Update of translations failed: ', err));
